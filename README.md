# Atividade de Automação Prova Bossa Box
## Projeto Automação Web Bossa Boz

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

O Projeto consiste na automatização do site  [Bossa Box.](https://dev.app.bossabox.com/login)
## Test end 2 end nas páginas
- Página de login
- Página de cadastro


## Tecnologia e Organização do Projeto

A Automação foi desenvolvida em Java (Maven) utilizando o Selenium WebDriver junto com o framework JUNIT.
O projeto foi dividido em alguns Packages:

- Core: Encontra-se toda a configuração e classes bases do projeto.
- Page: Encontram-se todas as chamadas do métodos da Classe DLS. O Page é onde está o mapeamento de cada pagina.
- Test: Encontram-se as classes de Testes. Dentro da pasta Test há uma pasta Suite aonde você consegue executar toda suite de testes.

Na Classe DSL estão os métodos comuns para todas as páginas.
Na Classe DriveFactory está a chamada dos drives. 
Fiz o mapeamento dos drives do Chrome e Firefox, e por default a chamada está sendo para o Chrome. Caso necessário ultilizar o Firefox, deve-se descomentar a linha:

```sh
//		System.setProperty("webdriver.gecko.driver", "target\\drives\\geckodriver.exe");
//		driver = new FirefoxDriver();
```

Os drivers encontram-se na Pasta target/drives dentro do projeto.
Para executar o Projeto, utilizar a IDE Eclipse ou Intellij, fazendo o import do projeto Maven.