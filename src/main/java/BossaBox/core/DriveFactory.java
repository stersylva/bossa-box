package BossaBox.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriveFactory {

    private static WebDriver driver;

    private void DriverFactory() {}

    public static WebDriver getDriver() {
        if(driver == null) {
//		System.setProperty("webdriver.gecko.driver", "target\\drives\\geckodriver.exe");
//		driver = new FirefoxDriver();

            System.setProperty("webdriver.chrome.driver", "target\\drives\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.get("https://dev.app.bossabox.com");
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static void killDriver() {
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
