package BossaBox.core;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import static BossaBox.core.DriveFactory.getDriver;

public class DSL {
    Actions actions = new Actions(getDriver());

    public void buttonClick(String xpath) {
        getDriver().findElement(By.xpath(xpath)).click();
    }

    public void setInput(String xpath, String text) {
        getDriver().findElement(By.xpath(xpath)).sendKeys(text);
    }
    public String getText(String xpath) {
        return getDriver().findElement(By.xpath(xpath)).getText();
    }
}
