package BossaBox.page;

import BossaBox.core.DSL;

public class LoginPage {
    private DSL dsl = new DSL();

    public void setEmail(String email) {
        dsl.setInput("//input[@id='input-email']", email);
    }

    public void clickBtnSignIn() {
        dsl.buttonClick("//button[contains(text(),'Entrar')]");
    }

    public void setPassword(String password) {
        dsl.setInput("//input[@id='input-password']", password);
    }

    public String getMessageError() {
        return dsl.getText("//body/div[@id='app']/div[2]/div[1]/div[1]");
    }

    public void register() {
        dsl.buttonClick("//button[contains(text(),'Cadastre-se')]");
    }

    public void forgotPassword() {
        dsl.buttonClick("//button[contains(text(),'Esqueceu a senha?')]");
    }

    public void registerGoogle() {
        dsl.buttonClick("//span[contains(text(),'Prosseguir com Google')]");
    }
}
