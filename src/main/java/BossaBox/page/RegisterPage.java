package BossaBox.page;

import BossaBox.core.DSL;

public class RegisterPage {
    private DSL dsl = new DSL();

    public void setEmail(String email) {
        dsl.setInput("//input[@id='input-email']", email);
    }

    public void setPassword(String password) {
        dsl.setInput("//input[@id='input-password']", password);
    }

    public String getMessageError(String msg) {
        return dsl.getText("//body/div[@id='app']//p[contains(text(),'" + msg + "')]");
    }

    public void setName(String name) {
        dsl.setInput("//input[@id='input-fullName']", name);
    }

    public void setConfirmPassword(String confirmPassword) {
        dsl.setInput("//input[@id='input-confirmPassword']", confirmPassword);
    }

    public void checkTerms() {
        dsl.buttonClick("//body/div[@id='app']/div[2]/div[1]/div[1]/div[2]/div[1]/label[5]/div[1]/div[1]");
    }

    public void clickBtnRegister() {
        dsl.buttonClick("//button[contains(text(),'Cadastrar')]");
    }
}
