package BossaBox.test.Suite;

import BossaBox.test.LoginTest;
import BossaBox.test.RegisterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        LoginTest.class,
        RegisterTest.class

})
public class SuiteTest {
}
