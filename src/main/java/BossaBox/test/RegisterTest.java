package BossaBox.test;

import BossaBox.page.LoginPage;
import BossaBox.page.RegisterPage;
import com.github.javafaker.Faker;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import static BossaBox.core.DriveFactory.getDriver;
import static BossaBox.core.DriveFactory.killDriver;

public class RegisterTest {

    private RegisterPage registerPage = new RegisterPage();
    private Faker faker = new Faker();
    private LoginPage loginPage = new LoginPage();

    @Before
    public void initialize() throws InterruptedException{
        Assert.assertEquals("BossaBox | Login", getDriver().getTitle());
        Thread.sleep(3000);
        loginPage.register();
        Assert.assertEquals("BossaBox | Cadastro", getDriver().getTitle());
    }

    @Test
    public void registerTest() throws InterruptedException {
        Thread.sleep(3000);
        registerPage.setPassword("12345678");
        registerPage.setConfirmPassword("12345678");
        registerPage.setName(faker.harryPotter().character());
        registerPage.setEmail(faker.random().nextInt(2, 100) + "@bossabox.com");
        registerPage.checkTerms();
        registerPage.clickBtnRegister();
        Thread.sleep(12000);
        Assert.assertEquals("BossaBox | Conclusão do cadastro", getDriver().getTitle());

    }

    @Test
    public void smallPasswordTest() throws InterruptedException {
        Thread.sleep(3000);
        registerPage.setPassword("12345");
        registerPage.setConfirmPassword("12345");
        registerPage.setName(faker.pokemon().name());
        registerPage.setEmail(faker.random().nextInt(100, 200) + "@bossabox.com");
        registerPage.checkTerms();
        Thread.sleep(3000);
        registerPage.clickBtnRegister();
        Thread.sleep(5000);
        String expectedErrorMessage = registerPage.getMessageError("A senha deve ter pelo menos 8 caracteres");
        Assert.assertEquals("A senha deve ter pelo menos 8 caracteres", expectedErrorMessage);

    }

    @Test
    public void passwordNotMatchTest() throws InterruptedException {
        Thread.sleep(3000);
        registerPage.setPassword("123452365");
        registerPage.setConfirmPassword("12345");
        registerPage.setName(faker.pokemon().name());
        registerPage.setEmail(faker.random().nextInt(100, 200) + "@bossabox.com");
        registerPage.checkTerms();
        Thread.sleep(3000);
        registerPage.clickBtnRegister();
        Thread.sleep(5000);
        String expectedErrorMessage = registerPage.getMessageError("As senhas não correspondem");
        Assert.assertEquals("As senhas não correspondem", expectedErrorMessage);

    }

    @Test
    public void emailFieldTest() throws InterruptedException {
        Thread.sleep(3000);
        registerPage.setPassword("12345678");
        registerPage.setConfirmPassword("12345678");
        registerPage.setName(faker.pokemon().name());
        registerPage.setEmail(faker.random().nextInt(100, 200) + "bossabox.com");
        registerPage.checkTerms();
        Thread.sleep(3000);
        registerPage.clickBtnRegister();
        Thread.sleep(5000);
        String expectedErrorMessage = registerPage.getMessageError("E-mail e/ou senha inválidos");
        Assert.assertEquals("E-mail e/ou senha inválidos", expectedErrorMessage);

    }


    @After
    public void finish() {
        killDriver();
    }
}
