package BossaBox.test;

import BossaBox.page.LoginPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import static BossaBox.core.DriveFactory.getDriver;
import static BossaBox.core.DriveFactory.killDriver;

public class LoginTest {
    private LoginPage loginPage;

    @Before
    public void initialize() {
        loginPage = new LoginPage();
        Assert.assertEquals("BossaBox | Login", getDriver().getTitle());
    }
    @Test
    public void notUserCorrectTest() throws InterruptedException {
        Thread.sleep(3000);
        loginPage.setEmail("teste@bossabox.com");
        loginPage.setPassword("123456");
        loginPage.clickBtnSignIn();
        Thread.sleep(3000);
        String errorMessage = "Usuário/senha incorreto";
        String expectedErrorMessage = loginPage.getMessageError();
        Assert.assertEquals(errorMessage, expectedErrorMessage);

    }

    @Test
    public void userRegisteredTest() throws InterruptedException {
        Thread.sleep(3000);
        loginPage.register();
        Assert.assertEquals("BossaBox | Cadastro", getDriver().getTitle());
    }

    @Test
    public void userForgotPasswordTest() throws InterruptedException {
        Thread.sleep(3000);
        loginPage.forgotPassword();
        Assert.assertEquals("BossaBox | Recuperação de senha", getDriver().getTitle());
    }

    @Test
    public void userRegisteredGoogleTest() throws InterruptedException {
        Thread.sleep(3000);
        loginPage.registerGoogle();
        Assert.assertEquals("BossaBox | Login", getDriver().getTitle());
    }

    @After
    public void finish() {
        killDriver();
    }
}
